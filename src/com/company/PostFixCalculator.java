package com.company;

import java.util.Scanner;
import java.util.Stack;

public class PostFixCalculator {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input expression in POSTfix notation using space: ");
        String str = scanner.nextLine();
        scanner.close();

        String [] strings = str.split(" ");
        Stack<Integer> stack = new Stack<Integer>();

        for (int i = 0; i <strings.length; i++) {

            if(isNumber(strings[i])) {
                stack.push(Integer.valueOf(strings[i]));
            } else {
                double temp1 = stack.pop();
                double temp2 = stack.pop();

                switch (strings[i]){
                    case "+":
                        stack.push((int) (temp1 + temp2));
                        break;
                    case "-":
                        stack.push((int) (temp2 - temp1));
                        break;
                    case "*":
                        stack.push((int) (temp1 * temp2));
                        break;
                    case "/":
                        stack.push((int) (temp1 / temp2));
                        break;
                }
            }


        }
        if (!stack.empty()){
            System.out.println(stack.pop());
        } else {
            System.out.println("Error");
        }


    }



    private static boolean isNumber(String string) {

        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException exception){
            return false;
        }



    }

}