package com.company;

import java.util.Scanner;

public class PretfixCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input expression in PREfix notation using space: ");
        System.out.println("value = " + calculate(scanner));
        scanner.close();
    }


    public static int calculate(Scanner input) {
        if (input.hasNextDouble()) {
            return input.nextInt();
        } else {
            String operator = input.next();
            int operand1 = calculate(input);
            int operand2 = calculate(input);
            return calculate(operator, operand1, operand2);
        }
    }

    public static int calculate(String operator, int operand1,
                                int operand2) {
        if (operator.equals("+")) {
            return operand1 + operand2;
        } else if (operator.equals("-")) {
            return operand1 - operand2;
        } else if (operator.equals("*")) {
            return operand1 * operand2;
        } else if (operator.equals("/")) {
            return operand1 / operand2;
        } else {
            throw new RuntimeException("illegal operator " + operator);
        }
    }
}